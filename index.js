"use strict"

/*
    1. Напишіть функцію, яка повертає частку двох чисел.
    Виведіть результат роботи функції в консоль.
 */

let firstNumber = null;
let secondNumber = null;

do {

    firstNumber = +prompt("Enter first number");
    if (!Number.isInteger(firstNumber)) {
        alert("You have not entered a number!!!");
    } else {
        secondNumber = +prompt("Enter second number");
    }

} while (!Number.isInteger(secondNumber))

const getDivision = (a, b) => a / b;

alert(getDivision(firstNumber, secondNumber));
console.log(getDivision(firstNumber, secondNumber));


/*
    2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції
    з введеними користувачем числами.

 */



let userNumberFirst = null;
let userNumberSecond = null;
let userChoice = null;

do {
    userNumberFirst = +prompt("Enter your number");
    if (!Number.isInteger(userNumberFirst)) {
        alert("You have not entered a number");
    } else {
        userChoice = prompt("Enter a math action here '+,-,/,*' ")
        if (!(userChoice === '+' || userChoice === '-' || userChoice === '/' || userChoice === '*')) {
            alert("You have not entered a single math action");
        } else {
            userNumberSecond = +prompt("Enter your second number");
        }
    }
} while (!Number.isInteger(userNumberSecond))

const getMathOperation = (firstNum, mathOperation, secondNum) => {
    let result;
    if (mathOperation === '+') {
        result = firstNum + secondNum;
    } else if (mathOperation === '-') {
        result = firstNum - secondNum;
    } else if (mathOperation === '/') {
        result = firstNum / secondNum;
    } else {
        result = firstNum * secondNum;
    }
    return result;
}

alert(getMathOperation(userNumberFirst, userChoice, userNumberSecond));
console.log(getMathOperation(userNumberFirst, userChoice, userNumberSecond));



function factorial(n) {
    if (n === 0 || n === 1) {
        return 1; // Факторіал 0 та 1 дорівнює 1
    }

    let result = 1;
    for (let i = 2; i <= n; i++) {
        result *= i;
    }

    return result;
}

let result = factorial(5);
console.log(result);
